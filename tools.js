var timers = {};

function onTimer(name) {
    if (timers[name] == undefined)timers[name]={'d_':0,'data':[]}
    if (!timers[name].d_) {
        timers[name].d_=Date.now()
    }
    else {
        var d = Date.now() - timers[name].d_;
        timers[name].data.push(d)
        timers[name].d_=0;
    }
    
}

function getTimers() {
   ret = ''
   for (var n in timers) {

   var sum=0;
    for (var item of timers[n].data ) {
        sum =sum+item;
    }
     if (timers[n])ret = ret + n +':' + Math.round( (sum / timers[n].data.length)  *10) + " | \n";
     
     timers[n].data = [];

   }
   return ret;
}