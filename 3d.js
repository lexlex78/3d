var canvas = document.getElementById('canvas');
var stat = document.getElementById('stat');
var ctx = canvas.getContext('2d');

canvas.width = canvas.clientWidth;
canvas.height = canvas.clientHeight;
 //ctx.scale(canvas.width, canvas.height);

var canvasWidth = canvas.width;
var canvasHeight = canvas.height;
var canvasWidth_ = canvas.width/2;
var canvasHeight_ = canvas.height/2;

var d = canvasWidth_;

var rad =  2 * Math.PI / 360;
var sin = [];
var cos = [];
for (var i=0;i<=360;i++) {
    sin.push(Math.sin(rad * i));
    cos.push(Math.cos(rad * i));
}



function clear_scr() {
    ctx.fillStyle = '#fff';
    ctx.fillRect(0,0,canvasWidth,canvasHeight);
}

function point(x, y , cplor){
    ctx.strokeStyle = cplor;
    ctx.strokeRect(x,y,0.5,0.5);
}

function triangle(x, y, x1, y1, x2, y2 , r , g , b){
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.closePath();
    ctx.fillStyle = `rgb(${r},${g},${b})`;
    ctx.fill();
}

function triangle_(x, y, x1, y1, x2, y2 , color){
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.closePath();
    // ctx.lineWidth =2;
    // ctx.strokeStyle = 0;
    ctx.stroke();
    ctx.fillStyle = color;
    ctx.fill();

}



var model_cub = {
    vertexes:[
        [10,  10,  10],
        [-10,  10,  10],
        [-10,  -10,  10],
        [10,  -10,  10],
        [10,  10,  -10],
        [-10,  10,  -10],
        [-10,  -10,  -10],
        [10,  -10,  -10],
    ],
    triangles:[
        [0, 1, 2],
        [0, 2, 3,],
        [4, 0, 3],
        [4, 3, 7,],
        [5, 4, 7,],
        [5, 7, 6,],
        [1, 5, 6,],
        [1, 6, 2,],
        [4, 5, 1,],
        [4, 1, 0,],
        [2, 6, 7,],
        [2, 7, 3,],
    ]
}

function rotX (x,y,z , a) {
    a=Math.round(a)
    x_ = x;
    y_ = y*cos[a] - z*sin[a];
    z_ = y*sin[a] + z*cos[a];
    return [x_,y_,z_];
}

function rotY (x,y,z , a) {
    a=Math.round(a)
    x_ = x*cos[a] + z*sin[a];
    y_ = y;
    z_ = -x*sin[a] + z*cos[a];
    return [x_,y_,z_];
}

function rotZ (x,y,z , a) {
    a=Math.round(a)
    x_ = x*cos[a] - y*sin[a];
    y_ = x*sin[a] + y*cos[a];
    z_ = z;
    return [x_,y_,z_];
}

var rn = canvasHeight_;
var lx=ly = 0 - rn;
var lx_=canvasWidth+rn;
var ly_=canvasHeight+rn;

function ProjectVertex(x,y,z) {
    if (z<=0 || z>4000) return []
    var rx = (x * d / z) + canvasWidth_ ;
    var ry = (y * d / z) + canvasHeight_;
    if (rx<lx || ry<ly || rx > lx_ ||  ry > ly_  ) return [];

    return [rx , ry ];
}

function setModel( model , x,y,z , zoom ,ax ,ay ,az ) {
   // var model_ = Object.assign({}, model)
    var model_ = JSON.parse(JSON.stringify(model));

    if (zoom !=1)
    for (item of model_.vertexes) {
        item[0] = item[0] * zoom ;
        item[1] = item[1] * zoom ;
        item[2] = item[2] * zoom ;
    }

    if (ax>0 && ax<360)
    for (item of model_.vertexes) {
        var r =  rotX(...item,ax);
        item[0] = r[0];item[1] = r[1];item[2] = r[2];
    }

    if (ay>0 && ay<360)
        for (item of model_.vertexes) {
            var r =  rotY(...item,ay);
            item[0] = r[0];item[1] = r[1];item[2] = r[2];
        }

    if (az>0 && az<360)
        for (item of model_.vertexes) {
            var r =  rotZ(...item,az);
            item[0] = r[0];item[1] = r[1];item[2] = r[2];
        }

    for (item of model_.vertexes) {
        item[0] = item[0] + x - camera[0];
        item[1] = item[1] + y - camera[1];
        item[2] = item[2] + z - camera[2];
    }


    //camera rotation
    if (camera[3]>0 && camera[3]<360)
    for (item of model_.vertexes) {
        var r =  rotX(...item,360-camera[3]);
        item[0] = r[0];item[1] = r[1];item[2] = r[2];
    }
    if (camera[4]>0 && camera[4]<360)
    for (item of model_.vertexes) {
        var r =  rotY(...item,360-camera[4]);
        item[0] = r[0];item[1] = r[1];item[2] = r[2];
    }
    if (camera[5]>0 && camera[5]<360)
    for (item of model_.vertexes) {
        var r =  rotZ(...item,360-camera[5]);
        item[0] = r[0];item[1] = r[1];item[2] = r[2];
    }

    return model_
}

/// point in triangle tx,ty,tz , normal vector (vx,vy,vz) 
function getK (tx,ty,tz , vx,vy,vz) {
    var lx=light[0]-tx;
    var ly=light[1]-ty;
    var lz=light[2]-tz;
    
    l_v = lx*vx+ly*vy+lz*vz;
    lv_d = (lx*lx+ly*ly+lz*lz) * (vx*vx+vy*vy+vz*vz); 
    
//    return l_v/Math.sqrt(lv_d);
   return l_v*Math.abs(l_v)/lv_d;
}

function getVector (x,y,z,x1,y1,z1,x2,y2,z2) {

   var  ax = x1-x; var  ay = y1-y; var  az = z1-z;
   var  bx = x2-x; var  by = y2-y; var  bz = z2-z;

   return [ay*bz-az*by , az*bx-ax*bz , ax*by-ay*bx]
}

function renderScene() {


    var arrayTR = [];

    for (model of scene) {
        var ver = [];
        for (v of model.vertexes){
            ver.push(ProjectVertex(...v ));
        }

        
       

        
        /// render
        for (t of model.triangles){
            if (ver[t[0]].length != 0 && ver[t[1]].length != 0 && ver[t[2]].length != 0 ) {
                
                var vector = getVector(... model.vertexes[t[0]] ,... model.vertexes[t[1]] , ... model.vertexes[t[2]]); 
                 
               

                // point (...ProjectVertex(model.vertexes[t[0]][0] + (vector[0]/100 )  , model.vertexes[t[0]][1] + (vector[1]/100)  , model.vertexes[t[0]][2]  + (vector[2]/100)  ),'#000')

            //  if(vector[2] <= 0) {
                if(getK (...model.vertexes[t[0]],...vector) > 0) {
                var rgb = [0,255,0];
                // var k=0.3+vector[2]/-7000
                var k=0.5 + getK (...model.vertexes[t[0]],...vector)/2;
                if (k>1)k=1;
                rgb = rgb.map(function(val){
                    return val*k;
                })
                 arrayTR.push([...ver[t[0]] ,...ver[t[1]] , ...ver[t[2]] , ...rgb , model.vertexes[t[0]][2] +  model.vertexes[t[1]][2] + model.vertexes[t[2]][2]] );
             }
              
            }
        }
    }

    arrayTR.sort(function (tb,ta) {
            return ta[9] - tb[9];
    })


    for (t of arrayTR ) {
        triangle (...t);
    }

}

var scene = [];
var render_process = false;
var a=0;

var cubs= [
       [model_cub , 1,1,1,0,0,0 , -400,0,0,0,0,0],
    //  [model_cub , 0,0,0,0,0,0 , 30,0,-300,0,0,0]
];
// camera position (x,y,z) rot (a,b,l)
var camera = [0,0,-500,0,0,0]
// light position (x,y,z)
var light = [0,0,0]

function rand(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

function cloneCube() {
    for (var a=1;a<=100;a++) {
        cubs.push([model_cub,rand(-5,5),rand(-5,5),rand(-5,5),rand(-5,5),rand(-5,5),rand(-5,5) ,rand(-1*canvasHeight_,canvasHeight_) ,rand(-1*canvasHeight_,canvasHeight_) ,rand(-1*canvasHeight_,canvasHeight_) ,0,0,0])
    }
}

        cloneCube();

function mov(x,dx) {
    var triger = 1;
    x=x+dx;
    if (x>canvasHeight_ || x< (canvasHeight_ * -1)) triger = -1;
    return [x,triger]
}

function rot(x,dx) {
    x=x+dx;
    if(x>360)x=x-360;
    if(x<0)x=x+360;
   return x;
}

function active(cub) {
    var ret = [];
    
    ret = mov(cub[7],cub[4]);
    cub[7] = ret[0];
    cub[4] = cub[4]*ret[1];

    ret = mov(cub[8],cub[5]);
    cub[8] = ret[0];
    cub[5] = cub[5]*ret[1];

    ret = mov(cub[9],cub[6]);
    cub[9] = ret[0];
    cub[6] = cub[6]*ret[1];

    cub[10] = rot(cub[10] ,cub[1]);
    cub[11] = rot(cub[11] ,cub[2]);
    cub[12] = rot(cub[12] ,cub[3]);

}

function render () {
    render_process = true;


    for (cub of cubs) {
        active (cub);
    }

    onTimer('SceneON');

    // camera[5]=camera[5]+1

    for (cub of cubs) {
        scene.push(setModel(cub[0], cub[7], cub[8],cub[9]  , 3 , cub[10] , cub[11] ,cub[12] ));
    }


    onTimer('SceneON');

    onTimer('renderScene');
    renderScene();
    onTimer('renderScene');
    scene = [];

    render_process =false
}

var sc=0;
function ran() {
    sc++;if (sc==30){
        sc=0;stat.textContent= getTimers();
    }
    if (!render_process) {
        onTimer('All');
        clear_scr()
        render()
        onTimer('All');
    }
    requestAnimationFrame(ran);
}

  requestAnimationFrame(ran);
 // render();